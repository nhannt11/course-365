//  Khai báo thư viện express
const express = require('express');

//Import courseMiddleware
const { courseMiddleware } = require('../middleware/courseMiddleware');

//Import course Controller
const { createCourse, getAllCourse, getCourseById, updateCourseById, deleteCourseById } = require('../controllers/courseController')


// Khai báo router
const courseRouter = express.Router();

// Sử dụng middleware
courseRouter.use(courseMiddleware);

courseRouter.get('/courses', getAllCourse);

courseRouter.post('/courses', createCourse);

courseRouter.get('/courses/:courseId', getCourseById);

courseRouter.put('/courses/:courseId', updateCourseById);

courseRouter.delete('/courses/:courseId', deleteCourseById);


//Export
module.exports = courseRouter;