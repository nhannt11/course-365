const gUserTable = ["courseCode", "courseName", "price", "discountPrice", "duration", "level", "coverImage", "teacherName", "teacherPhoto", "isPopular", "isTrending", "action"];
var vObjectCourses = [];
var gCourseCode_COL = 0;
var gCourseName_COL = 1;
var gPrice_COL = 2;
var gDiscountPrice_COL = 3;
var gDuration_COL = 4;
var gLevel_COL = 5;
var gCoverImage_COL = 6;
var gTeacherName_COL = 7;
var gTeacherPhoto_COL = 8;
var gIsPopular_COL = 9;
var gIsTrending_COL = 10;
var gAction_COL = 11;
vIdCourseDelete = [];
vIdCourseEdit = [];
vCourseCode = [];
$(document).ready(function () {
    callApiGetServer();
    onPageLoading();
    loadDataToTable(vObjectCourses.data);
    $("#create-courses").on('click', function () {
        onBtnCreateCourse();
    });
    $("#btn-insert-course").on('click', function () {
        onBtnConfirmCreateCourse();
    });
    $(document).on("click", "#data-table .update-btn", function () {
        onBtnEditClick(this)
    });
    $(document).on("click", "#data-table .delete-btn", function () {
        onBtnDeleteClick(this)
    });
    $("#btn-edit-course").on('click', function () {
        onBtnClickConfirmUpdate();
    });
    $("#btn-delete-course").on('click', function () {
        onBtnClickDeleteCourse();
    })
});
//function tải lại trang
function onPageLoading() {
    // thiết lập data table
    $("#data-table").DataTable({
        columns: [
            { data: gUserTable[gCourseCode_COL] },
            { data: gUserTable[gCourseName_COL] },
            { data: gUserTable[gPrice_COL] },
            { data: gUserTable[gDiscountPrice_COL] },
            { data: gUserTable[gDuration_COL] },
            { data: gUserTable[gLevel_COL] },
            { data: gUserTable[gCoverImage_COL] },
            { data: gUserTable[gTeacherName_COL] },
            { data: gUserTable[gTeacherPhoto_COL] },
            { data: gUserTable[gIsPopular_COL] },
            { data: gUserTable[gIsTrending_COL] },
            { data: gUserTable[gAction_COL] },
        ],
        columnDefs: [
            {
                targets: gAction_COL,
                className: "w-25 text-center",
                defaultContent: "<div class='row'><div class='col-6'><button class='btn btn-primary update-btn'><i class='fas fa-edit'></i> Sửa</button></div> <button class='btn btn-danger delete-btn'><i class='fas fa-eraser'></i> Xóa</button></div></div>"
            },
            {
                targets: gCoverImage_COL,
                className: "text-center",
                render: function (data, type, row, neta) {
                    return '<img class="img-thumbnail" src="' + data + '" height="100" width="130"/>';
                }
            },
            {
                targets: gTeacherPhoto_COL,
                className: "text-center",
                render: function (data, type, row, neta) {
                    return '<img class="img-thumbnail" src="' + data + '" height="100" width="130"/>';
                }
            },
        ]
    });
}
// get all data
function callApiGetServer() {
    $.ajax({
        url: "/courses",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (responseJson) {
            console.log(responseJson);
            vObjectCourses = responseJson
            console.log(vObjectCourses.data)
        },
        error: function (ajaxContent) {
            console.log(ajaxContent);
        }
    })
}
// create course modal
function onBtnCreateCourse() {
    $("#create-user").modal('show');
}
// comfirm create thêm mới course
function onBtnConfirmCreateCourse() {
    // tạo biến lưu thông tin từ input
    var gConfirmButton = {
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: "",
        isTrending: ""
    };
    // đọc thông tin từ input
    readDataFromCreate(gConfirmButton);
    // hàm validate
    var vCheckData = validateDataConfirm(gConfirmButton);
    // nếu validate = true thì chạy tiếp
    if (vCheckData == true) {
        // thêm data vào bảng
        callApiPostData(gConfirmButton)
        console.log(gConfirmButton)
        // load trang
        loadDataToTable(vObjectCourses.data);
        alert("INSERT thông tin thành công!");
        // xóa trắng bảng
        resetCreateModal();
        // ẩn modal
        $("#create-user").modal('hide');
    }
}
function callApiPostData(paramConfirmButton) {
    $.ajax({
        type: "POST",
        url: "/courses",
        data: JSON.stringify(paramConfirmButton),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (res) {
            console.log(res)
            location.reload();
        },
        error: function (res) {
            console.log(res.status)
        }
    });
}
// nút sửa modal
function onBtnEditClick(paramUpdateButton) {
    // show modal
    $("#Replace-user").modal('show');
    //loadModal
    loadDataToUpdateModal(paramUpdateButton);
}
// function hiển thi, thay đổi course, update
function onBtnClickConfirmUpdate() {
    // biến lưu thông tin update
    var gConfirmButtonUpdate = {
        id: "",
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: "",
        isTrending: ""
    };
    // đọc thông tin từ input
    readDataUpdate(gConfirmButtonUpdate);
    // hàm validate thông tin
    var vCheckData = validateUpdateConfirm(gConfirmButtonUpdate);
    // nếu thông tin nhập vào hợp lệ
    if (vCheckData) {
        // update thông tin mới
        updateDataCourse(gConfirmButtonUpdate);
        // api update
        callApiUpdate(gConfirmButtonUpdate)
        // load lại bảng
        loadDataToTable(vObjectCourses.data);
        alert("UPDATE thông tin thành công!");
        // xóa modal
        resetUpdateModal();
        // ẩn modal
        $("#Replace-user").modal('hide');
    }
}
// function deleta course
function onBtnDeleteClick(paramDeleteButton) {
    $("#delete-course-modal").modal('show');
    var vCurrentTr = $(paramDeleteButton).closest("tr");
    var vTable = $("#data-table").DataTable();
    var vDataRow = vTable.row(vCurrentTr).data();
    vCourseCode = vDataRow.courseCode
    var vObjectRequest = vObjectCourses.data
    vObjectRequest.filter(function (index, objectData) {
        if (index.courseCode === vCourseCode) {
            vIdCourseDelete = index._id
            console.log(vIdCourseDelete)
        }
    })
}
// function delete course
function onBtnClickDeleteCourse() {
    callApiDelete()
    // load lại bảng
    loadDataToTable(vObjectCourses.data)
    // ẩn modal 
    $("#delete-course-modal").modal("hide")
}

// delete course
function callApiDelete() {
    $.ajax({
        url: "/courses/" + vIdCourseDelete,
        type: "DELETE",
        dataType: "json",
        success: function (reponseJson) {
            console.log(reponseJson);
            alert("DELETE thông tin thành công!");
            location.reload();
        },
    })
}
// lấy thông tin được điền từ input
function readDataFromCreate(paramConfirmButton) {
    paramConfirmButton.courseCode = $("#input-coursecode").val().trim();
    paramConfirmButton.courseName = $("#input-coursename").val().trim();
    paramConfirmButton.price = Number($("#input-price").val().trim());
    paramConfirmButton.discountPrice = Number($("#input-discount").val().trim());
    paramConfirmButton.duration = $("#input-duration").val().trim();
    paramConfirmButton.level = $("#input-level").val().trim();
    paramConfirmButton.coverImage = $("#input-coverimage").val().trim();
    paramConfirmButton.teacherName = $("#input-teachername").val().trim();
    paramConfirmButton.teacherPhoto = $("#input-teacherphoto").val().trim();
    paramConfirmButton.isPopular = $("#input-popular").val().trim();
    paramConfirmButton.isTrending = $("#input-trending").val().trim();
}
// hàm validate thông tin thêm
function validateDataConfirm(paramConfirmButton) {
    var vObject = vObjectCourses.data
    if (paramConfirmButton.courseCode === "") {
        alert("chưa nhập Course code!");
        return false;
    }
    if (paramConfirmButton.courseName === "") {
        alert("chưa nhập Course Name!");
        return false;
    }
    if (paramConfirmButton.price === "" || paramConfirmButton.price < 0) {
        alert("chưa nhập giá tiền hoặc nhập sai");
        return false;
    }
    if (paramConfirmButton.discountPrice <= 0) {
        alert("Discount Price sai!");
        return false;
    }
    if (paramConfirmButton.duration === "") {
        alert("chưa nhập duration!");
        return false;
    }
    if (paramConfirmButton.level === "") {
        alert("chưa nhập level!");
        return false;
    }
    if (paramConfirmButton.coverImage === "") {
        alert("chưa nhập coverImage!");
        return false;
    }
    if (paramConfirmButton.teacherName === "") {
        alert("chưa nhập teacherName!");
        return false;
    }
    if (paramConfirmButton.teacherPhoto === "") {
        alert("chưa nhập teacherPhoto!");
        return false;
    }
    if (paramConfirmButton.isPopular === "no-pick") {
        alert("Vui lòng chọn isPopular!");
        return false;
    }
    if (paramConfirmButton.isTrending === "no-picktrend") {
        alert("Vui lòng chọn isTrending!");
        return false;
    }
    return true;
}
// hàm validate cho update function
function validateUpdateConfirm(paramConfirmButton) {
    var vObject = vObjectCourses.data
    if (paramConfirmButton.courseCode === "") {
        alert("chưa nhập Course code!");
        return false;
    }
    if (paramConfirmButton.courseName === "") {
        alert("chưa nhập Course Name!");
        return false;
    }
    if (paramConfirmButton.price === "" || paramConfirmButton.price < 0) {
        alert("chưa nhập giá tiền hoặc nhập sai");
        return false;
    }
    if (paramConfirmButton.discountPrice <= 0) {
        alert("Discount Price sai!");
        return false;
    }
    if (paramConfirmButton.duration === "") {
        alert("chưa nhập duration!");
        return false;
    }
    if (paramConfirmButton.level === "") {
        alert("chưa nhập level!");
        return false;
    }
    if (paramConfirmButton.coverImage === "") {
        alert("chưa nhập coverImage!");
        return false;
    }
    if (paramConfirmButton.teacherName === "") {
        alert("chưa nhập teacherName!");
        return false;
    }
    if (paramConfirmButton.teacherPhoto === "") {
        alert("chưa nhập teacherPhoto!");
        return false;
    }
    if (paramConfirmButton.isPopular === "no-pick") {
        alert("Vui lòng chọn isPopular!");
        return false;
    }
    if (paramConfirmButton.isTrending === "no-picktrend") {
        alert("Vui lòng chọn isTrending!");
        return false;
    }
    return true;
}
// update api
function callApiUpdate(paramConfirmButtonUpdate) {
    console.log()
    $.ajax({
        type: "PUT",
        url: "/courses/" + vIdCourseEdit,
        dataType: "json",
        data: JSON.stringify(paramConfirmButtonUpdate),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            console.log(data);
        },
    })
}
// function xóa trắng bảng create
function resetCreateModal() {
    $("#input-id").val("");
    $("#input-coursecode").val("");
    $("#input-coursename").val("");
    $("#input-price").val("");
    $("#input-discount").val("");
    $("#input-duration").val("");
    $("#input-level").val("");
    $("#input-coverimage").val("");
    $("#input-teachername").val("");
    $("#input-teacherphoto").val("");
    $("#input-popular").val("no-pick");
    $("#input-trending").val("no-picktrend");
}
// load data lên update modal
function loadDataToUpdateModal(paramUpdate) {
    var vCurrentTr = $(paramUpdate).closest("tr");
    var vTable = $("#data-table").DataTable();
    var vDataRow = vTable.row(vCurrentTr).data();
    vCourseCode = vDataRow.courseCode
    console.log(vCourseCode)
    var vObjectRequest = vObjectCourses.data
    vObjectRequest.filter(function (index, objectData) {
        console.log(index)
        if (index.courseCode === vCourseCode) {
            vIdCourseEdit = index._id
            console.log(vIdCourseEdit)
        }
    })
    $("#input-coursecode-edit").val(vDataRow.courseCode);
    $("#input-coursename-edit").val(vDataRow.courseName);
    $("#input-price-edit").val(vDataRow.price);
    $("#input-discount-edit").val(vDataRow.discountPrice);
    $("#input-duration-edit").val(vDataRow.duration);
    $("#input-level-edit").val(vDataRow.level);
    $("#input-coverimage-edit").val(vDataRow.coverImage);
    $("#input-teachername-edit").val(vDataRow.teacherName);
    $("#input-teacherphoto-edit").val(vDataRow.teacherPhoto);
    $("#input-popular-edit").val(String(vDataRow.isPopular));
    $("#input-trending-edit").val(String(vDataRow.isTrending));
}
// đọc dữ liệu update modal
function readDataUpdate(paramConfirmButtonUpdate) {
    paramConfirmButtonUpdate.id = vIdCourseEdit;
    paramConfirmButtonUpdate.courseCode = $("#input-coursecode-edit").val();
    paramConfirmButtonUpdate.courseName = $("#input-coursename-edit").val();
    paramConfirmButtonUpdate.price = $("#input-price-edit").val();
    paramConfirmButtonUpdate.discountPrice = $("#input-discount-edit").val();
    paramConfirmButtonUpdate.duration = $("#input-duration-edit").val();
    paramConfirmButtonUpdate.level = $("#input-level-edit").val();
    paramConfirmButtonUpdate.coverImage = $("#input-coverimage-edit").val();
    paramConfirmButtonUpdate.teacherName = $("#input-teachername-edit").val();
    paramConfirmButtonUpdate.teacherPhoto = $("#input-teacherphoto-edit").val();
    paramConfirmButtonUpdate.isPopular = $("#input-popular-edit").val();
    paramConfirmButtonUpdate.isTrending = $("#input-trending-edit").val();
}
// xóa trắng update modal
function resetUpdateModal() {
    $("#input-id-edit").val("");
    $("#input-coursecode-edit").val("");
    $("#input-coursename-edit").val("");
    $("#input-price-edit").val("");
    $("#input-discount-edit").val("");
    $("#input-duration-edit").val("");
    $("#input-level-edit").val("");
    $("#input-coverimage-edit").val("");
    $("#input-teachername-edit").val("");
    $("#input-teacherphoto-edit").val("");
    $("#input-popular-edit").val("no-pick");
    $("#input-trending-edit").val("no-picktrend");
}
// hàm sửa thông tin khóa học đã sửa vào obj
function updateDataCourse(paramConfirmButtonUpdate) {
    for (var i = 0; i < vObjectCourses.data.length; i++) {
        if (vObjectCourses.data[i].id === paramConfirmButtonUpdate.id) {
            vObjectCourses.data[i].courseCode = paramConfirmButtonUpdate.courseCode;
            vObjectCourses.data[i].courseName = paramConfirmButtonUpdate.courseName;
            vObjectCourses.data[i].price = paramConfirmButtonUpdate.price;
            vObjectCourses.data[i].discountPrice = paramConfirmButtonUpdate.discountPrice;
            vObjectCourses.data[i].duration = paramConfirmButtonUpdate.duration;
            vObjectCourses.data[i].level = paramConfirmButtonUpdate.level;
            vObjectCourses.data[i].coverImage = paramConfirmButtonUpdate.coverImage;
            vObjectCourses.data[i].teacherName = paramConfirmButtonUpdate.teacherName;
            vObjectCourses.data[i].teacherPhoto = paramConfirmButtonUpdate.teacherPhoto;
            vObjectCourses.data[i].isPopular = paramConfirmButtonUpdate.isPopular;
            vObjectCourses.data[i].isTrending = paramConfirmButtonUpdate.isTrending;
        }
    }
}
// load to table
function loadDataToTable(paramCoursesDB) {
    var vTable = $("#data-table").DataTable();
    vTable.clear();
    vTable.rows.add(paramCoursesDB);
    vTable.draw();
}