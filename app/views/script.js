
var vObjectCourses = []
// ham load trang
$(document).ready(function () {
  onPageLoading();

});
// onpageloading
function onPageLoading() {
  callApiGetServer();
  loadCardDataPopular();
  loadCardDataTrending();
}
// call api
function callApiGetServer() {
  $.ajax({
    url: "/courses",
    type: "GET",
    dataType: "json",
    async: false,
    success: function (responseJson) {
      console.log(responseJson);
      vObjectCourses = responseJson
    },
    error: function (ajaxContent) {
      console.log(ajaxContent);
    }
  })
}
// ad card data popular
function loadCardDataPopular() {
  var popularCard = "";
  $(vObjectCourses).each(function (index, course) {
    for (var i = 0; i < course.data.length; i++) {
      if (course.data[i].isPopular == true) {
        popularCard += `
                <div class="col-3">
                  <div class="card" style="width: 120%;">
                    <img class="card-img-top" src="` + course.data[i].coverImage + `" alt="Card image cap">
                  <div class="card-body" id="popular-list">
                        <h5 class="card-title">` + course.data[i].courseName + `</h5>
                        <p class="card-text">` + `<i class="far fa-clock"></i>` + course.data[i].duration + " " + course.data[i].level + `</p>
                        <p class="card-text">` + "$" + course.data[i].discountPrice + `<del>` + "$" + course.data[i].price + `</del>` + `</p>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                          <div class="col-sm-3 mt-2">
                            <img src="` + course.data[i].teacherPhoto + `" class="rounded-circle" width="120%" alt="">
                          </div>
                          <div class="col-sm-7 mt-4">
                            <p>`+ course.data[i].teacherName + `</p>
                          </div>
                          <div class="col-sm-1 mt-4">
                            <i class="far fa-bookmark"></i> </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                   `
        $('#popular-list').html(popularCard);
      }
    }
  })
}
// ad card data trending
function loadCardDataTrending() {
  var trendingCard = "";
  $(vObjectCourses).each(function (index, course) {
    for (var i = 0; i < course.data.length; i++) {
      if (course.data[i].isTrending == true) {
        trendingCard += `
                <div class="col-3">
                  <div class="card" style="width: 120%;">
                    <img class="card-img-top" src="` + course.data[i].coverImage + `" alt="Card image cap">
                  <div class="card-body" id="popular-list">
                        <h5 class="card-title">` + course.data[i].courseName + `</h5>
                        <p class="card-text">` + `<i class="far fa-clock"></i>` + course.data[i].duration + " " + course.data[i].level + `</p>
                        <p class="card-text">` + "$" + course.data[i].discountPrice + `<del>` + "$" + course.data[i].price + `</del>` + `</p>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                          <div class="col-sm-3 mt-2">
                            <img src="` + course.data[i].teacherPhoto + `" class="rounded-circle" width="120%" alt="">
                          </div>
                          <div class="col-sm-7 mt-4">
                            <p>`+ course.data[i].teacherName + `</p>
                          </div>
                          <div class="col-sm-1 mt-4">
                            <i class="far fa-bookmark"></i> </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                   `
        $('#trending-card').html(trendingCard);
      }
    }
  })
}

